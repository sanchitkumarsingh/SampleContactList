//
//  ContactTableView.h
//  SampleContactList
//
//  Created by Sanchit Kumar Singh on 6/16/16.
//  Copyright © 2016 Sanchit Kumar Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"

@protocol cellTappeddelegate <NSObject>

-(void)performFurtherActionOnItem:(Person *)aPerson;

@end


@interface ContactTableView : UITableView
-(id) initWithFrame:(CGRect)frame;
@property (strong,nonatomic) NSArray *contactList;
@property (weak,nonatomic) id <cellTappeddelegate> cellTapDelegate;
@end
