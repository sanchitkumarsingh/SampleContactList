//
//  DataManager.m
//  SampleContactList
//
//  Created by Sanchit Kumar Singh on 6/16/16.
//  Copyright © 2016 Sanchit Kumar Singh. All rights reserved.
//

#import "DataManager.h"

@interface DataManager()

@end

@implementation DataManager

+(DataManager*)sharedInstance {
    static DataManager *_sharedInstance;
    if(!_sharedInstance) {
        static dispatch_once_t oncePredicate;
        dispatch_once(&oncePredicate, ^{
            _sharedInstance = [[super allocWithZone:nil] init];
            // Init the dictionary
            
        });
    }
    return _sharedInstance;
}

+(id)allocWithZone:(NSZone *)zone {
    return [self sharedInstance];
}

-(id)copyWithZone:(NSZone *)zone {
    return self;
}

-(void) getDataFromUrl:(NSString *)url withCompletion:(void(^)(NSArray *dataArray))completion{
    [self fetchJsonObjectForUrl:url success:^(id data) {
        if ([data isKindOfClass:[NSArray class]]){
            NSArray *personsArray = (NSArray *)data;
            NSMutableArray *persons = [[NSMutableArray alloc]init];
            for (NSDictionary *personDict in personsArray){
                Person *person = [self arrangePersonData:personDict andPersonObject:nil];
                [persons addObject:person];
            }
            completion([self makeDataArray:persons]);
        }
    } failure:^(NSError *error) {
        //<#code#>
    }];

}

-(Person *) arrangePersonData:(NSDictionary *)personDictionary andPersonObject:(Person *)aPerson{
    if(aPerson){
        if([personDictionary objectForKey:@"first_name"]){
            NSString *firstName = [personDictionary objectForKey:@"first_name"];
            NSString *capFirstName = [firstName stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                                                      withString:[[firstName substringToIndex:1] capitalizedString]];
            aPerson.firstName=capFirstName;
        }
        aPerson.lastName=[personDictionary objectForKey:@"last_name"];
        aPerson.email=[personDictionary objectForKey:@"email"];
        aPerson.phoneNumber=[personDictionary objectForKey:@"phone_number"];
        if([personDictionary objectForKey:@"favorite"]){
            if([[personDictionary objectForKey:@"favorite"] boolValue] == TRUE)
                aPerson.isFavorite=@"True";
            else
                aPerson.isFavorite=@"False";
        }
        aPerson.createdAt=[personDictionary objectForKey:@"created_at"];
        aPerson.updatedAt=[personDictionary objectForKey:@"updated_at"];
        aPerson.profilePicture=[personDictionary objectForKey:@"profile_pic"];
        aPerson.personId=[personDictionary objectForKey:@"id"];
        return aPerson;
    }
    Person *person = [[Person alloc]init];
    if([personDictionary objectForKey:@"first_name"]){
        NSString *firstName = [personDictionary objectForKey:@"first_name"];
        NSString *capFirstName = [firstName stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                                                    withString:[[firstName substringToIndex:1] capitalizedString]];
        person.firstName=capFirstName;
    }
    person.lastName=[personDictionary objectForKey:@"last_name"];
    person.email=[personDictionary objectForKey:@"email"];
    person.phoneNumber=[personDictionary objectForKey:@"phone_number"];
    person.isFavorite=[personDictionary objectForKey:@"favorite"];
    person.createdAt=[personDictionary objectForKey:@"created_at"];
    person.updatedAt=[personDictionary objectForKey:@"updated_at"];
    person.profilePicture=[personDictionary objectForKey:@"profile_pic"];
    person.personId=[personDictionary objectForKey:@"id"];
    return person;
}


-(void)fetchPersonDataForPerson:(Person *)aPerson withSuccess:(void(^) (Person *Person))success andfailure:(void(^)(NSError *error))failure{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://gojek-contacts-app.herokuapp.com/contacts/%@.json",aPerson.personId]];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:urlRequest];
    operation.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if([responseObject isKindOfClass:[NSDictionary class]]){
            NSDictionary *personDict = (NSDictionary *)responseObject;
            success([self arrangePersonData:personDict andPersonObject:aPerson]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    [operation start];

}

-(NSArray *) applyFilterOnDataArray:(NSArray *)dataArray withParam:(NSString *)parameter{
    NSString* filter = @"%K BEGINSWITH[cd] %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:filter, @"firstName", parameter];
    return [dataArray filteredArrayUsingPredicate:predicate];
}

-(NSArray *)makeDataArray:(NSArray *)dataArray{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"firstName"
                                                 ascending:YES selector:@selector(caseInsensitiveCompare:)];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [dataArray sortedArrayUsingDescriptors:sortDescriptors];
    return sortedArray;
}

-(void)fetchJsonObjectForUrl:(NSString *)urlString success:(void(^)(id data))success failure:(void(^) (NSError *error)) failure{
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:urlRequest];
    operation.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    [operation start];
}

-(void)addToContactList:(NSString *)urlString withParameters:(NSDictionary *)params success:(void(^)(id data))success failure:(void(^)(NSError *error))failure{
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache"};
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://gojek-contacts-app.herokuapp.com/contacts.json"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                        failure(error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        success(response);
                                                        NSLog(@"%@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)updateContactForPersonId:(NSString *)key withParameters:(NSDictionary *)paramDict withCompletion:(void(^)(id response))success{
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache"};
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:paramDict options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://gojek-contacts-app.herokuapp.com/contacts/%@.json",key]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"PUT"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        success(response);
                                                        NSLog(@"%@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)getPersonDisplayPicture:(NSString *)url withCompletion:(void (^) (UIImage *image))completionBlock{
    if([url isEqualToString:@"/images/missing.png"]){
        completionBlock([UIImage imageNamed:@"Display_placeholder"]);
        return;
    }
    NSURL *imageUrl = [NSURL URLWithString:url];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageUrl];
    AFHTTPRequestOperation* readOp = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest
                                      ];
    AFImageResponseSerializer* responseSerializer = [AFImageResponseSerializer serializer];
    responseSerializer.imageScale = 1;
    readOp.responseSerializer = responseSerializer;
    [readOp setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,UIImage *image) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock (image);
        });
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
    [readOp start];
    
}

@end
