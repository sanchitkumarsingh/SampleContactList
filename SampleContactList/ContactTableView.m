//
//  ContactTableView.m
//  SampleContactList
//
//  Created by Sanchit Kumar Singh on 6/16/16.
//  Copyright © 2016 Sanchit Kumar Singh. All rights reserved.
//

#import "ContactTableView.h"
#import "ContactTableCell.h"
#import "LoaderTableCell.h"

@implementation ContactTableView

-(id) initWithFrame:(CGRect)frame{
    self =[super initWithFrame:frame];
    if(self){
        self.dataSource=self;
        self.delegate=self;
    }
    [self registerCell];
    [self setBackgroundColor:[UIColor clearColor]];
    self.separatorStyle=UITableViewCellSeparatorStyleNone;
    return self;
}

-(void)registerCell{
    [self registerNib:[UINib nibWithNibName:@"ContactTableCell" bundle:nil] forCellReuseIdentifier:@"ContactTableCell"];
    [self registerNib:[UINib nibWithNibName:@"LoaderTableCell" bundle:nil] forCellReuseIdentifier:@"LoaderTableCell"];
}

#pragma mark - Table view DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.contactList.count)
        return self.contactList.count;
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell...
    if(!self.contactList.count){
        LoaderTableCell *loaderCell = [tableView dequeueReusableCellWithIdentifier:@"LoaderTableCell" forIndexPath:indexPath];
        return loaderCell;
    }
    ContactTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactTableCell" forIndexPath:indexPath];
    BOOL showSectionTitle = NO;
    if(indexPath.row>0){
        Person *person1 = [self.contactList objectAtIndex:indexPath.row];
        Person *person2 = [self.contactList objectAtIndex:indexPath.row-1];
        if(!([[person1.firstName substringToIndex:1] isEqualToString:[person2.firstName substringToIndex:1]])){
            showSectionTitle = YES;
        }
    }else if (indexPath.row==0){
        showSectionTitle = YES;
    }
    [cell bindData:[self.contactList objectAtIndex:indexPath.row] withSectionTitle:showSectionTitle];
    return cell;
}

-(CGFloat) tableView:(UITableView *) tableView heightForHeaderInSection :(NSInteger) section{
    return 0.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

#pragma mark - Table view delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.cellTapDelegate performFurtherActionOnItem:[self.contactList objectAtIndex:indexPath.row]];
}

@end
