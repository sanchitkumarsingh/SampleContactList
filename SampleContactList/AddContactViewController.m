//
//  AddContactViewController.m
//  SampleContactList
//
//  Created by Sanchit Kumar Singh on 6/19/16.
//  Copyright © 2016 Sanchit Kumar Singh. All rights reserved.
//

#import "AddContactViewController.h"
#import "DataManager.h"
#import "SVProgressHUD.h"
@interface AddContactViewController ()
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIImageView *displayPicture;
@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *mobileNUmber;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UIScrollView *containerView;


@end

@implementation AddContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.displayPicture.layer.cornerRadius = CGRectGetHeight(self.displayPicture.frame)/2;
    [self.view setBackgroundColor:RGBA(185, 185, 185, 1)];
    [self.containerView setBackgroundColor:[UIColor clearColor]];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    
    //adhering notifications for keyboard action
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    self.displayPicture.layer.cornerRadius=CGRectGetHeight(self.displayPicture.bounds)/2;
    self.displayPicture.image = [UIImage imageNamed:@"Display_placeholder"];
    self.displayPicture.clipsToBounds=YES;
    [self.containerView setScrollEnabled:NO];
    self.firstName.delegate=self;
    self.lastName.delegate=self;
    self.email.delegate=self;
    self.mobileNUmber.delegate=self;
    [self setTitle:@"Add Contact"];
    [self setBackNavigationButton];
    [self.displayPicture setUserInteractionEnabled:YES];
    [self.containerView bringSubviewToFront:self.displayPicture];
    UITapGestureRecognizer *displayPictureTapped = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(uploadPicture:)];
    [self.displayPicture addGestureRecognizer:displayPictureTapped];
    [self.view bringSubviewToFront:self.displayPicture];
//    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
//                                                         forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)setBackNavigationButton{
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0,0.0, 39.0,39.0)];
    leftButton.showsTouchWhenHighlighted = TRUE;
    [leftButton addTarget:self action:@selector(backTapped:) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"Back_nav.png"] forState:UIControlStateNormal];
    leftButton.imageEdgeInsets = UIEdgeInsetsMake(0, -30, 0, 0);
    leftButton.titleLabel.text=@"";
    leftButton.titleLabel.textColor=[UIColor redColor];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [self.navigationItem setLeftBarButtonItem:leftBarButton];
}

-(void)uploadPicture:(UITapGestureRecognizer *)sender{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.delegate = self;
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //You can retrieve the actual UIImage
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    //Or you can get the image url from AssetsLibrary
    NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];
    [picker dismissViewControllerAnimated:YES completion:nil];
    self.displayPicture.image = image;
}

-(void) backTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    //CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    for(UITextField *textField in self.containerView.subviews){
        if([textField isFirstResponder]){
            self.containerView.contentOffset = CGPointMake(0, textField.frame.origin.y-100);
        }
    }
}

-(void)keyboardWillHide:(NSNotification *)notification
{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.containerView.frame = f;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    NSInteger nextTag = textField.tag+1;
    UIResponder *nextResponder = [textField.superview viewWithTag:nextTag];
    if(nextResponder)
        [nextResponder becomeFirstResponder];
    else
        [textField resignFirstResponder];
    return NO;
}

- (IBAction)saveButtonPressed:(id)sender {
    [SVProgressHUD show];
    if([self.firstName.text length]<3||[self.lastName.text length]<1||[self.mobileNUmber.text length]<10){
        //show alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"invalid contact"
                                                        message:@"Please fill correctly"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [SVProgressHUD dismiss];
    }
    else{
    NSDictionary *paramsDict = @{@"first_name":self.firstName.text,@"last_name":self.lastName.text,@"phone_number":self.mobileNUmber.text,@"email":self.email.text,@"favorite":@"false"};
    [[DataManager sharedInstance]addToContactList:BASE_URL withParameters:paramsDict success:^(id data) {
        //<#code#>
        [SVProgressHUD dismiss];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                        message:@"Contact added"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        } failure:^(NSError *error) {
        //<#code#>
        [SVProgressHUD dismiss];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Failure"
                                                        message:@"Contact Not added.Please Try again"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

    }];
    [self.navigationController popViewControllerAnimated:YES];

    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
