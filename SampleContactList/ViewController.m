//
//  ViewController.m
//  SampleContactList
//
//  Created by Sanchit Kumar Singh on 6/16/16.
//  Copyright © 2016 Sanchit Kumar Singh. All rights reserved.
//

#import "ViewController.h"
#import "DataManager.h"
#import "ContactTableView.h"
#import "ContactDetailViewController.h"
#import "AddContactViewController.h"

@interface ViewController ()<cellTappeddelegate>
@property (weak, nonatomic) IBOutlet UIButton *addContactButton;
@property (strong,nonatomic) ContactTableView *contactTable;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Contact Book";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    UIButton *aButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
    [aButton setBackgroundImage:[UIImage imageNamed:@"User.png"] forState:UIControlStateNormal];
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc]initWithCustomView:aButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    [self.navigationController.navigationBar setBarTintColor:RGBA(62, 80, 180, 1)];
    self.addContactButton.layer.cornerRadius = CGRectGetHeight(self.addContactButton.bounds)/2;
    [self.view setBackgroundColor:RGBA(250, 250, 250, 1)];
}

-( void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if(!self.contactTable){
        self.contactTable = [[ContactTableView alloc]initWithFrame:self.view.frame];
        [self.view addSubview:self.contactTable];
        self.contactTable.cellTapDelegate=self;
    }
    [[DataManager sharedInstance]getDataFromUrl:BASE_URL withCompletion:^(NSArray *dataArray) {
        self.contactTable.contactList=dataArray;
        [self.contactTable reloadData];
    }];
    [self.view bringSubviewToFront:self.addContactButton];
}

-(void)performFurtherActionOnItem:(Person *)aPerson{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContactDetailViewController *contactDetailVC = [storyBoard instantiateViewControllerWithIdentifier:@"ContactDetailViewController"];
    contactDetailVC.person=aPerson;
    [self.navigationController pushViewController:contactDetailVC animated:YES];
    [[DataManager sharedInstance]fetchPersonDataForPerson:aPerson withSuccess:^(Person *Person) {
       // <#code#>
        contactDetailVC.person=Person;
        [contactDetailVC setUpViewsForItem:Person];
    } andfailure:^(NSError *error) {
       // <#code#>
    }];
}
- (IBAction)addContactButtonTapped:(id)sender {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddContactViewController *addContactVC = [storyBoard instantiateViewControllerWithIdentifier:@"AddContactViewController"];
    [self.navigationController pushViewController:addContactVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
