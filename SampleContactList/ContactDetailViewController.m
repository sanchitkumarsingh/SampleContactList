//
//  ContactDetailViewController.m
//  SampleContactList
//
//  Created by Sanchit Kumar Singh on 6/19/16.
//  Copyright © 2016 Sanchit Kumar Singh. All rights reserved.
//

#import "ContactDetailViewController.h"
#import "DataManager.h"
#import "SVProgressHUD.h"
#import <MessageUI/MessageUI.h>

@interface ContactDetailViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *displayPicture;
@property (weak, nonatomic) IBOutlet UILabel *personName;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *email;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UIButton *phoneButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *contactLoader;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *emailLoader;


@end

@implementation ContactDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpViewsForItem:self.person];
    [self.view setBackgroundColor:RGBA(185, 185, 185, 1)];
    [self setTitle:@"Detail"];
    [self setBackNavigationButton];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.contactLoader startAnimating];
    [self.emailLoader startAnimating];
    // Do any additional setup after loading the view.
}

-(void)setBackNavigationButton{
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0,0.0, 39.0,39.0)];
    leftButton.showsTouchWhenHighlighted = TRUE;
    [leftButton addTarget:self action:@selector(backTapped:) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"Back_nav.png"] forState:UIControlStateNormal];
    leftButton.imageEdgeInsets = UIEdgeInsetsMake(0, -30, 0, 0);
    //leftButton.backgroundColor = [UIColor blackColor];
    leftButton.titleLabel.text=@"";
    leftButton.titleLabel.textColor=[UIColor redColor];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [self.navigationItem setLeftBarButtonItem:leftBarButton];
}

-(void) backTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) setUpViewsForItem : (Person *)person{
    if(person.displayPicture){
        self.displayPicture.image=person.displayPicture;
    }else{
        [[DataManager sharedInstance]getPersonDisplayPicture:person.profilePicture withCompletion:^(UIImage *image) {
            person.displayPicture=image;
            self.displayPicture.image = image;
        }];
    }
    self.displayPicture.layer.cornerRadius = CGRectGetHeight(self.displayPicture.frame)/2;
    self.displayPicture.clipsToBounds=YES;
    self.personName.text = [NSString stringWithFormat:@"%@ %@",person.firstName,person.lastName];
    if(person.phoneNumber){
        [self.contactLoader stopAnimating];
        [self.contactLoader removeFromSuperview];
        self.phoneNumber.text = person.phoneNumber;
    }
    if(person.email){
        [self.emailLoader stopAnimating];
        [self.emailLoader removeFromSuperview];
        self.email.text = person.email;
    }
    [self.favoriteButton setBackgroundColor:[UIColor clearColor]];
    if([person.isFavorite isEqualToString:@"True"]){
        [self.favoriteButton setBackgroundImage:[UIImage imageNamed:@"fav_active"] forState:UIControlStateNormal];
    }else{
        [self.favoriteButton setBackgroundImage:[UIImage imageNamed:@"fav_inactive"] forState:UIControlStateNormal];
    }
}
- (IBAction)favoriteButtonTapped:(id)sender {
    NSDictionary *paramDict;
    if ([self.person.isFavorite isEqualToString:@"True"]){
        [self.favoriteButton setBackgroundImage:[UIImage imageNamed:@"fav_inactive"] forState:UIControlStateNormal];
        self.person.isFavorite = @"False";
        paramDict = @{@"favorite":@false};
    }else{
        [self.favoriteButton setBackgroundImage:[UIImage imageNamed:@"fav_active"] forState:UIControlStateNormal];
        self.person.isFavorite = @"True";
        paramDict = @{@"favorite":@true};
    }
    [[DataManager sharedInstance]updateContactForPersonId:self.person.personId withParameters:paramDict withCompletion:^(id response) {
    }];
}
- (IBAction)phoneButton:(id)sender {
    if(self.phoneNumber.text){
        NSString *phoneNumber = [@"tel://" stringByAppendingString:self.phoneNumber.text];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }else{
        //show alert if needed.
    }
}
- (IBAction)emailButton:(id)sender {
    NSArray *toRecipents = [NSArray arrayWithObject:self.person.email];
    NSString *messageBody = @"Long time no see!!";
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setToRecipients:toRecipents];
    [mc setMessageBody:messageBody isHTML:NO];
    [self presentViewController:mc animated:YES completion:nil];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
