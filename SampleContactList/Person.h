//
//  Person.h
//  SampleContactList
//
//  Created by Sanchit Kumar Singh on 6/18/16.
//  Copyright © 2016 Sanchit Kumar Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Person : NSObject

@property (retain,nonatomic) NSString *firstName;
@property (retain,nonatomic) NSString *lastName;
@property (retain,nonatomic) NSString *phoneNumber;
@property (retain,nonatomic) NSString *profilePicture;
@property (retain,nonatomic) NSString *email;
@property (retain,nonatomic) NSString *personId;
@property (retain,nonatomic) NSString *isFavorite;
@property (retain,nonatomic) NSString *createdAt;
@property (retain,nonatomic) NSString *updatedAt;
@property (strong,nonatomic) UIImage *displayPicture;



@end
