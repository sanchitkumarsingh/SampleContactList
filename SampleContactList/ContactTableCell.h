//
//  ContactTableCell.h
//  SampleContactList
//
//  Created by Sanchit Kumar Singh on 6/16/16.
//  Copyright © 2016 Sanchit Kumar Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"

@interface ContactTableCell : UITableViewCell

-(void)bindData:(Person *)personObject withSectionTitle:(BOOL) enabled;

@end
