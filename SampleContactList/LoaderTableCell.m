//
//  LoaderTableCell.m
//  SampleContactList
//
//  Created by Sanchit Kumar Singh on 6/29/16.
//  Copyright © 2016 Sanchit Kumar Singh. All rights reserved.
//

#import "LoaderTableCell.h"

@interface LoaderTableCell()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet UILabel *loaderLabel;

@end

@implementation LoaderTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setBackgroundColor:[UIColor clearColor]];
    [self.loaderLabel setBackgroundColor:[UIColor clearColor]];
    [self.loader startAnimating];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];

    // Configure the view for the selected state
}

@end
