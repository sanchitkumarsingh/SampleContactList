//
//  ContactDetailViewController.h
//  SampleContactList
//
//  Created by Sanchit Kumar Singh on 6/19/16.
//  Copyright © 2016 Sanchit Kumar Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"

@interface ContactDetailViewController : UIViewController

@property (strong,nonatomic) Person *person;
- (void) setUpViewsForItem : (Person *)person;

@end
