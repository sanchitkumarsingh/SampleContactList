//
//  Person.m
//  SampleContactList
//
//  Created by Sanchit Kumar Singh on 6/18/16.
//  Copyright © 2016 Sanchit Kumar Singh. All rights reserved.
//

#import "Person.h"

@implementation Person

-(instancetype) init
{
    self=[super init];
    if(self){
        self.firstName=@"";
        self.lastName=@"";
        self.isFavorite=@"";
        self.email=@"";
        self.createdAt=@"";
        self.updatedAt=@"";
        self.personId=@"";
        self.phoneNumber=@"";
        self.profilePicture=@"";
    }
    return self;
}

@end
