//
//  AppDelegate.h
//  SampleContactList
//
//  Created by Sanchit Kumar Singh on 6/16/16.
//  Copyright © 2016 Sanchit Kumar Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

