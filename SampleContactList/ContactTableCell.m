//
//  ContactTableCell.m
//  SampleContactList
//
//  Created by Sanchit Kumar Singh on 6/16/16.
//  Copyright © 2016 Sanchit Kumar Singh. All rights reserved.
//

#import "ContactTableCell.h"
#import "DataManager.h"

@interface ContactTableCell()
@property (weak, nonatomic) IBOutlet UILabel *sectionTitle;
@property (weak, nonatomic) IBOutlet UIImageView *contactImage;
@property (weak, nonatomic) IBOutlet UILabel *contactName;

@end

@implementation ContactTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)bindData:(Person *)personObject withSectionTitle:(BOOL) enabled{
    [self.sectionTitle setHidden:YES];
    self.contactName.text = [NSString stringWithFormat:@"%@ %@",personObject.firstName,personObject.lastName];
    self.contactImage.image = [UIImage imageNamed:@"Display_placeholder"];
    [[DataManager sharedInstance]getPersonDisplayPicture:personObject.profilePicture withCompletion:^(UIImage *image) {
        personObject.displayPicture=image;
        self.contactImage.image = image;
        self.contactImage.layer.cornerRadius = CGRectGetHeight(self.contactImage.bounds)/2;
        self.contactImage.clipsToBounds=YES;
    }];
    if(enabled){
        [self.sectionTitle setHidden:NO];
        self.sectionTitle.text = [personObject.firstName substringToIndex:1];
        [self.sectionTitle setTextColor:[UIColor redColor]];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];

    // Configure the view for the selected state
}

@end
