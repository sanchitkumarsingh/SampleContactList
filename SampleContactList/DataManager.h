//
//  DataManager.h
//  SampleContactList
//
//  Created by Sanchit Kumar Singh on 6/16/16.
//  Copyright © 2016 Sanchit Kumar Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "Person.h"

#define BASE_URL @"http://gojek-contacts-app.herokuapp.com/contacts.json"
#define RGBA(r, g, b, a)    [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
@interface DataManager : NSObject
+ (DataManager*)sharedInstance ;
-(void)getPersonDisplayPicture:(NSString *)url withCompletion:(void (^) (UIImage *image))completionBlock;
-(void) getDataFromUrl:(NSString *)url withCompletion:(void(^)(NSArray *dataArray))completion;
-(void)addToContactList:(NSString *)urlString withParameters:(NSDictionary *)params success:(void(^)(id data))success failure:(void(^)(NSError *error))failure;
-(void)fetchPersonDataForPerson:(Person *)aPerson withSuccess:(void(^) (Person *Person))success andfailure:(void(^)(NSError *error))failure;
-(void)updateContactForPersonId:(NSString *)key withParameters:(NSDictionary *)paramDict withCompletion:(void(^)(id response))success;
@end
