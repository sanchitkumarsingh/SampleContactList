//
//  DataManagerTests.m
//  SampleContactList
//
//  Created by Sanchit Kumar Singh on 6/20/16.
//  Copyright © 2016 Sanchit Kumar Singh. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DataManager.h"

@interface DataManager(test)
-(NSArray *)makeDataArray:(NSArray *)dataArray;
@end

@interface DataManagerTests : XCTestCase
@property (nonatomic) DataManager *dataManager;
@end

@implementation DataManagerTests

- (void)setUp {
    [super setUp];
    self.dataManager = [[DataManager alloc]init];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testGetPersonDisplayPicture{
    NSString *url = @"http://www.intrawallpaper.com/static/images/funky-wallpaper-hd_k99cFPc.jpg";
    [self measureBlock:^{
        [self.dataManager getPersonDisplayPicture:url withCompletion:^(UIImage *image) {
        }];
    }];
}

-(void) testMakeDataArray{
    NSMutableArray *expectedArray = [[NSMutableArray alloc]init];
    NSMutableArray *inputArray = [[NSMutableArray alloc]init];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    NSMutableDictionary *dict2 = [[NSMutableDictionary alloc]init];
    [dict setObject:@"Alpha" forKey:@"firstName"];
    [dict2 setObject:@"Beta" forKey:@"firstName"];
    [inputArray addObjectsFromArray:@[dict2,dict]];
    [expectedArray addObjectsFromArray:@[dict,dict2]];
    NSArray *outputArray = [self.dataManager makeDataArray:inputArray];
    XCTAssertEqualObjects(outputArray,expectedArray,@"wrong answer");
}


@end
